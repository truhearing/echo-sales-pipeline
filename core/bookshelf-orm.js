var knex = require('knex')({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'postgres',
    password: '',
    database: 'sales_pipeline',
    schema: 'echo',
    charset: 'utf8'
  }
});

module.exports = require('bookshelf')(knex);