var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var bookshelf = require('./bookshelf-orm');

/*
* @class Server
* @description The class that is responsible for loading in routes and setting up our express/socket.io servers
*/
var Server = (function() {

  /*
  * @function addRoutes
  * @description Loads in our route files and gives them the server dependencies they may require
  */
  Server.prototype.addRoutes = function() {
    var that = this;
    var routeDir = __dirname + '/../routes/';
    var routes = fs.readdirSync(routeDir);
    routes.forEach(function(file) {
      require(routeDir + file)(that.app, that.io, that.bookshelf);
    });
  };

  /*
  * @function configure
  * @description Sets up the express app to display errors and grabs the routes from our addRoutes function
  */
  Server.prototype.configure = function() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));

    this.addRoutes();

    /* Error Messages */

    // 404 Not Found
    this.app.use(function(req, res, next) {
      var err = new Error('Not Found');
      res.status(404).json({
        message: err.message,
        error: err
      });
    });

    // 500 Stack Trace
    this.app.use(function(err, req, res, next) {
      res.status(500).json({
        message: err.message,
        error: err
      });
    });
  };

  /*
  * @constructor
  */
  function Server() {
    this.app = express();
    this.http = require('http').Server(this.app);
    this.io = require('socket.io')(this.http);
    this.bookshelf = bookshelf;

    this.configure();

    this.http.listen(3002);
    console.log("Server listening on port 3002");
  }

  return Server;

})();

module.exports = Server;
