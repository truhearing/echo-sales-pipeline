module.exports = function(bookshelf) {

  var Contact = bookshelf.Model.extend({
    tableName: 'echo.contacts'
  });

  return Contact;

};
