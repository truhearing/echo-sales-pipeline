module.exports = function(bookshelf) {

  var Contact = require('./contact')(bookshelf);

  var Deal = bookshelf.Model.extend({
    tableName: 'echo.deals',
    contact: function() {
      return this.belongsTo(Contact, 'contact_id');
    }
  });

  return Deal;

};
