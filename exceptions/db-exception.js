/*
* @function DbException
* @params message:string
*/
function DbException(message) {
  this.message = message;
  this.name = 'DbException';
}

module.exports = DbException;
