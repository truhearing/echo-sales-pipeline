var _ = require('underscore')._;
var bind = require('../utils/bind');

var DealsRoutes = (function() {

  DealsRoutes.prototype.completeActivity = function(activity) {
    // Should probably store this to the DB
    console.log('Completing an existing activity.');
    this.dealShowRoom.emit('completed activity', activity);
  };

  DealsRoutes.prototype.createActivity = function(activity) {
    // Should probably store this to the DB
    console.log('Sending a new activity.');
    this.dealShowRoom.emit('created activity', activity);
  };

  DealsRoutes.prototype.initializeDealShowData = function(socket, id) {
    var that = this;

    this.ContactModel.fetchAll().then(function(contacts) {
      new that.DealModel().where({id: id}).fetch({withRelated: ['contact']}).then(function(deal) {
        socket.emit('initialized', {
          contacts: contacts.toJSON(),
          deal: deal.toJSON()
        });
      });
    });
  };

  DealsRoutes.prototype.closeDeal = function(params) {
    var that = this;

    new that.DealModel().where({id: params.deal.id}).fetch().then(function(deal) {
      deal.save({is_closed: 1}, {patch: true}).then(function(model) {
        var theDeal = model.toJSON();
        theDeal.contact = params.deal.contact;
        that.dealListRoom.emit('closed deal', theDeal);
      });
    });
  };

  DealsRoutes.prototype.changeDealStatus = function(params) {
    var that = this;

    new that.DealModel().where({id: params.deal.id}).fetch().then(function(deal) {
      deal.save({status: params.destStatus}, {patch: true}).then(function(model) {
        var theDeal = model.toJSON();
        theDeal.contact = params.deal.contact;
        that.dealListRoom.emit('changed status', theDeal, params.sourceStatus);
      });
    });
  };

  DealsRoutes.prototype.initializeDealListData = function(socket) {
    var that = this;

    this.ContactModel.fetchAll().then(function(contacts) {
      that.DealModel.where({is_closed: 0}).fetchAll({withRelated: ['contact']}).then(function(deals) {
        socket.emit('initialized', {
          contacts: contacts.toJSON(),
          deals: deals.toJSON()
        });
      });
    });
  };

  DealsRoutes.prototype.createDeal = function(deal) {
    // This lacks validation and everything that makes a request safe.
    // Good thing it's a prototype.
    var that = this;

    this.DealModel.forge(deal).save().then(function(model) {
      model.load(['contact']).then(function(model) {
        that.dealListRoom.emit('created deal', model.toJSON());
      });
    });
  };

  DealsRoutes.prototype.route = function() {
    var that = this;

    this.dealListRoom = this.io
      .of('/deals')
      .on('connection', function(socket) {

        console.log('A user connected to the Deal List Room.')

        socket.on('initialize', function() {
          that.initializeDealListData(socket);
        });
        socket.on('create deal', that.createDeal);
        socket.on('change status', that.changeDealStatus);
        socket.on('close deal', that.closeDeal);

        socket.on('disconnect', function() {
          console.log('A user disconnected from the Deal List Room.');
        });

      });

    this.dealShowRoom = this.io
      .of('/deals/show')
      .on('connection', function(socket) {

        console.log('A user connected to the Deal Show Room.');

        socket.on('initialize', function(id) {
          that.initializeDealShowData(socket, id);
        });
        socket.on('new activity', that.createActivity);
        socket.on('complete activity', that.completeActivity);

      });
  };

  function DealsRoutes(app, io, bookshelf) {
    this.route = bind(this.route, this);
    this.createDeal = bind(this.createDeal, this);
    this.initializeDealListData = bind(this.initializeDealListData, this);
    this.changeDealStatus = bind(this.changeDealStatus, this);
    this.closeDeal = bind(this.closeDeal, this);
    this.createActivity = bind(this.createActivity, this);
    this.completeActivity = bind(this.completeActivity, this);

    this.app = app;
    this.io = io;
    this.DealModel = require('../models/deal.js')(bookshelf);
    this.ContactModel = require('../models/contact.js')(bookshelf);
    this.routePrefix = '/deals';

    this.route();
  }

  return DealsRoutes;
})();

module.exports = function(app, io, bookshelf) {
  return new DealsRoutes(app, io, bookshelf);
};
