var Server = require('./core/server');

/*
* @class SalesPipelineServices
* @description The services driving the sales pipeline prototype
*/
var SalesPipelineServices = (function() {

  /*
  * @constructor
  */
  function SalesPipelineServices() {
    // Initialize / bootstrap app here
    this.server = new Server();
  }

  return SalesPipelineServices;

})();

module.exports = new SalesPipelineServices();
